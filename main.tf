#define the provide
provider "aws" {
    region = "us-east-2"
}

#create a virtual network
resource "aws_vpc" "group3_vpc_tf" {
    cidr_block = "10.0.0.0/16"
    tags = {
      "Name" = "group3_vpc_tf"
    }
}

#create a subnet
resource "aws_subnet" "group3_pub_subnet1" {
  
    tags = {
      "name" = "group3_pub_subnet1"
    }
    availability_zone = "us-east-2a"
    vpc_id = aws_vpc.group3_vpc_tf.id
    cidr_block = "10.0.0.0/24"
    map_public_ip_on_launch = true
    depends_on = [
      aws_vpc.group3_vpc_tf
    ]
}

#Create subnet2
resource "aws_subnet" "group3_pub_subnet2" {
    tags = {
      "name" = "group3_pub_subnet2"
    }
    availability_zone = "us-east-2b"
    vpc_id = aws_vpc.group3_vpc_tf.id
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = true
    depends_on = [
      aws_vpc.group3_vpc_tf
    ]
}

#define route table
resource "aws_route_table" "group3_rt_tf" {
    tags = {
      "Name" = "group3_rt_tf"
    }
    vpc_id = aws_vpc.group3_vpc_tf.id
}
#associate subnet with route table
resource "aws_route_table_association" "my_rt_association" {
    subnet_id = aws_subnet.group3_pub_subnet1.id
    route_table_id = aws_route_table.group3_rt_tf.id
}
resource "aws_route_table_association" "my_rt_association2" {
    subnet_id = aws_subnet.group3_pub_subnet2.id
    route_table_id = aws_route_table.group3_rt_tf.id
}
#create default route in routing table
resource "aws_route" "default_route" {
    route_table_id = aws_route_table.group3_rt_tf.id
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.group3_IG.id
}

#create internet gateway
resource "aws_internet_gateway" "group3_IG" {
    tags = {
      "name" = "group3_IGW"
    }
    vpc_id = aws_vpc.group3_vpc_tf.id
    depends_on = [
      aws_vpc.group3_vpc_tf
    ]
}

#define the ec2 instance
resource "aws_instance" "group3server" {
    ami = "ami-0578f2b35d0328762"
    instance_type = "t2.micro"
    subnet_id = aws_subnet.group3_pub_subnet1.id
    vpc_security_group_ids = [aws_security_group.group3server_ec2_SG.id]
    user_data = "${file("userdata.sh")}"
    key_name = "backend-key"
    associate_public_ip_address = true
    tags = {
      "Name" = "group3server"
    }
    depends_on = [
      aws_subnet.group3_pub_subnet1,
      aws_security_group.group3server_ec2_SG
    ]
}

#create security group3 for ec2
resource "aws_security_group" "group3server_ec2_SG" {
    name = "group3server_ec2_SG"
    description = "allow web inbound traffic"
    vpc_id = aws_vpc.group3_vpc_tf.id
# Ingress rules
  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "cardealership3"
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Egress rules
  egress {
    description = "All traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#create security group3 for ALB
resource "aws_security_group" "group3server_ALB_SG" {
    name = "group3server_ALB_SG"
    description = "allow web inbound traffic"
    vpc_id = aws_vpc.group3_vpc_tf.id
}

#create loadbalancer
resource "aws_lb" "group3-alb" {
  name               = "group3-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.group3server_ec2_SG.id]
  subnets            = [aws_subnet.group3_pub_subnet1.id, aws_subnet.group3_pub_subnet2.id]

  enable_deletion_protection = true

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_listener" "group3-alb-listener2" {
  load_balancer_arn = aws_lb.group3-alb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "group3-alb-listener" {
  load_balancer_arn = aws_lb.group3-alb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:acm:us-east-2:126552587903:certificate/b5543968-bfb1-41d7-9b23-d714f54149ff"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.group3-alb-tg.arn
  }
}

# data "aws_acm_certificate" "cert_arn" {
#  domain = "boykeme.com"
# }
resource "aws_lb_listener_certificate" "group3-alb-listener-cert" {
  listener_arn    = aws_lb_listener.group3-alb-listener.arn
  certificate_arn = "arn:aws:acm:us-east-2:126552587903:certificate/b5543968-bfb1-41d7-9b23-d714f54149ff"
} 

resource "aws_lb_target_group" "group3-alb-tg" {
    name        = "tf-group3-alb-tg"
    target_type = "instance"
    port        = 80
    protocol    = "HTTP"
    vpc_id      = aws_vpc.group3_vpc_tf.id
    health_check {
    healthy_threshold   = 2
    interval            = 30
    protocol            = "HTTP"
    timeout             = 10
    unhealthy_threshold = 2
  }
}

resource "aws_lb_target_group_attachment" "group3-alb-tg-attach" {
  target_group_arn = aws_lb_target_group.group3-alb-tg.arn
  target_id        = aws_instance.group3server.id
  port             = 9000
}

# terraform aws data hosted zone
data "aws_route53_zone" "boykeme_zone" {
    name = "boykeme.com"
}

# # # create a record set in route 53
# # # terraform aws route 53 record
resource "aws_route53_record" "site_domain" {
  zone_id = data.aws_route53_zone.boykeme_zone.zone_id
  name    = "boykeme.com"
  type    = "A"
 
 
 alias {
   name = aws_lb.group3-alb.dns_name
   zone_id = aws_lb.group3-alb.zone_id
   evaluate_target_health = true
 }
}