#!/bin/bash
# Connect to your server via ssh and install the necessary dependencies into it
sudo su
sudo yum update
# Install git to clone the repository into your server
sudo yum install git -y
#create gitlab token
git config --global user.name "techchakfellow"

git config --global credential.gitlab.com.token <token>
# Clone your repository into your server
git clone https://gitlab.com/techchak-apprenticeship1/techchak-car-dealer-backend-app-team-3
cd techchak-dealership-backend-app-team-3
# Install node
sudo yum install curl
curl -sL https://rpm.nodesource.com/setup_16.x | sudo -E bash -
sudo yum install nodejs -y
# Using `npm install` within the application directory on your new server, you’ll install the node modules required to run your application. (NB: at this point, you can connect to your API using your ec2 IP address but we’re going to take it a step further by setting up a load balancer that will help us balance traffic coming from the internetgit clone https://github.com/techchak/techchak-dealership-backend-app-team-3.git
npm install
# Run your application using `pm2` by installing it on your server
npm install pm2 -g && pm2 update
echo /
"NODE_ENV=development /
PORT=5000 /
MONGODB_URI=mongodb+srv://techchakfellow:buGTHm76sr50Db39@techchakprojects.k7oct.mongodb.net/techchak-project?retryWrites=true&w=majority /
JWT_SECRET=SomeRandomStringHash /
JWT_EXPIRE=30d /
JWT_COOKIE_EXPIRE=30" > .env
# pm2 is a great piece of software that allows you to start, persist and monitor your Node application. You can also use `pm2 startup` to create the correct startup scripts to start your application on server reboot.
pm2 start sever.js
npm run dev
pm2 save
pm2 startup
# Set up your application load balancer with a target group pointing to your ec2 IP
# Once this is done, grab the DNS name of your load balancer, and paste the below into your browser one after the other